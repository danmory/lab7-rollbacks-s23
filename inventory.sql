CREATE TABLE Inventory (
    username TEXT REFERENCES Player(username),
    product TEXT REFERENCES Shop(product),
    amount INT CHECK(amount >= 0),
    PRIMARY KEY (username, product)
);

CREATE OR REPLACE FUNCTION check_inventory_limit() RETURNS TRIGGER AS $$
DECLARE
    total_products INT;
BEGIN
    SELECT SUM(amount) INTO total_products FROM Inventory WHERE username = NEW.username;
    IF total_products > 100 THEN
        RAISE EXCEPTION 'Player cannot store more than 100 products in their inventory';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER inventory_limit_trigger
BEFORE INSERT ON Inventory
FOR EACH ROW
EXECUTE FUNCTION check_inventory_limit();