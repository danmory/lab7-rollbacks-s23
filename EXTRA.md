# Extra task

## Part 1

If a function fails after the transaction has been committed,
the database will not be aware of the failure and the changes made by the transaction will remain committed
(Durability Property of Transactions).

If the client retries the request, the transaction will execute again, potentially resulting in duplicate records.
To overcome this problem we may assign to each transaction unique ID.
If we receive transaction with ID that was already proccessed then we can skip it.

The SQL script to create table with commited transactions:

```sql
CREATE TABLE CommittedTransactions (
    id SERIAL PRIMARY KEY,
    transaction_id VARCHAR(50) NOT NULL,
    timestamp TIMESTAMP NOT NULL DEFAULT NOW()
);
```

And python snippet to check transaction id:

```python
cur.execute("SELECT * FROM CommittedTransactions WHERE transaction_id = %s", (transaction_id,))
result = cur.fetchone()
if result is not None:
  ...
```

## Part 2

If we work with 2+ storages then we may use:

1. [Two-phase commit protocol](https://en.wikipedia.org/wiki/Two-phase_commit_protocol): In this approach, a coordinator node ensures that all participating nodes are ready to commit, then asks them to commit simultaneously. If any node fails, the coordinator instructs them all to roll back.

2. [Three-phase commit protocol](https://en.wikipedia.org/wiki/Three-phase_commit_protocol): an extension of two-phase commit (2PC) that adds a "pre-commit" phase between the "prepare" and "commit" phases of 2PC. During this phase, the coordinator node confirms that all participants can commit the transaction before actually committing it.

3. [Saga pattern](https://docs.aws.amazon.com/prescriptive-guidance/latest/modernization-data-persistence/saga-pattern.html)
